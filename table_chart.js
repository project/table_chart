(function($) {
    Drupal.behaviors.table_chart = {
      attach: function(context, settings) {
        var count = 0;
        $('.table-chart').each(function(){
          count++;
          $(this).once(function(){
            var wrapper = $(this);
            var table = wrapper.find('table');
            var chartLibrary = table.data('charting-library');

            // Check if the charting library and based on that formulate the table from the json object to how we can formulate it.
            switch(chartLibrary) {
              case 'chartist':
                chartist(wrapper, table, count);
                break;
              default:
              case 'morris':
                morrisJs(wrapper, table, count);
                break;
            }
          });
        });
      }
    };

    /**
     * Convert table into json array.
     */
    function prepareTableData(table, options = [], chartlib = 'morris') {
      // Ignoring the first column (i.e. 0) has some odd behavior. Zero (0)
      // being misinterpreted as the emtpy array value. So we treat this
      // case separately.
      if (table.data('tabletojson-ignoreColumns') == 0) {
        options.ignoreColumns = [0];
      }
      else {
        options.ignoreColumns = table.data('tabletojson-ignoreColumns') ? String(table.data('tabletojson-ignoreColumns')).split(',') : [];
      }
      // Same issue as above
      if (table.data('tabletojson-onlyColumns') == 0) {
        options.onlyColumns = [0];
      }
      else {
        options.onlyColumns = table.data('tabletojson-onlyColumns') ? table.data('tabletojson-onlyColumns').split(',') : null;
      }
      options.ignoreHiddenRows = table.data('tabletojson-ignoreHiddenRows') ? true == table.data('tabletojson-ignoreHiddenRows') : true;
      options.headings = table.data('tabletojson-headings') ? table.data('tabletojson-headings').split(',') : null;
      options.keys = [];
      // Data preperation

      // Ensure all the values are true integers and not string numbers
      options.ignoreColumns = options.ignoreColumns.map(function (x) {
        return parseInt(x, 10);
      });

      // Read the table data with the given configuration
      options.table_data = table.tableToJSON({
        ignoreColumns: options.ignoreColumns,
        onlyColumns: options.onlyColumns,
        ignoreHiddenRows: options.ignoreHiddenRows,
        headings: options.headings
      });

      return options;
    }

    // Chartist data formulate data.
    function chartist(wrapper, table, count) {
      var options = prepareTableData(table, options, 'chartist');
      var headings = [];
      var data_labels = [];
      var data_series = [];
      var series_num = 0;
      var series_template = [];

      // Tranpose the data to load into labels/series.
      options.table_data = transpose(options.table_data);

      // Grab the list of labels first.
      data_labels = options.table_data[Object.keys(options.table_data)[0]];

      // Based on the number of labels, build the series arrays.
      for(x = 1; x < Object.keys(options.table_data).length; x++) {
        data_series.push(options.table_data[Object.keys(options.table_data)[x]]);
      }

      // Create the chartis json object to create the markup.
      var data = {
        labels: data_labels,
        series: data_series,
      };
      var optKeys = [
        ['seriesBarDistance', 'int', 15],
        ['low', 'int', undefined],
        ['high', 'int', undefined],
        ['width', 'str', undefined],
        ['height', 'str', undefined],
        ['referenceValue', 'int', 0],
        ['stackBars', 'bool', false],
        ['stackMode', 'str', 'accumulate'],
        ['horizontalBars', 'bool', false],
        ['distributeSeries', 'bool', false],
        ['reverseData', 'bool', false],
        ['showGridBackground', 'bool', false],
        ['showLine', 'bool', true],
        ['showPoint', 'bool', true],
        ['showArea', 'bool', false],
        ['areaBase', 'int', 0],
        ['lineSmooth', 'bool', true],
        ['showGridBackground', 'bool', false],
        ['fullWidth', 'bool', false],
        ['reverseData', 'false', false],
        ['chartPadding', 'int', 5],
        ['startAngle', 'int', 0],
        ['total', 'int', undefined],
        ['donut', 'bool', false],
        ['donutSolid', 'bool', false],
        ['donutWidth', 'bool', 60],
        ['showLabel', 'bool', true],
        ['labelOffset', 'int', 0],
        ['labelPosition', 'str', 'inside'],
        ['labelDirection', 'str', 'neutral'],
        ['reverseData', 'bool', false],
        ['ignoreEmptyValues', 'bool', false],
      ];

      options = generateOptionsList(optKeys, table, 'chartist');
      options.element = 'chartist-chart-' + count;

      // Prepare display
      table.addClass('hidden');

      wrapper.append('<div id="' + options.element + '" class="table-chart-chartist-div"></div>');
      // Create a new line chart object where as first parameter we pass in a selector
      // that is resolving to our chart container element. The Second parameter
      // is the actual data object.
      var chartType = table.data('chartist-type');

      switch(chartType.toLowerCase()) {
        case 'bar':
          new Chartist.Bar('#' + options.element, data, options);
          break;
        default:
        case 'line':
          new Chartist.Line('#' + options.element, data, options);
          break;
        case 'pie':
          // The pie data is formulated differently.
          var pie_data = {
            series: data_series[0],
          };
          new Chartist.Pie('#' + options.element, pie_data, options);
          break;
      }
    }

    /**
     * Matrix transpose function.
     * 
     * @param {Array} data 
     * @returns array
     */
    function transpose(data) {
      var result = {};
      for (let row of data) {
        for (var [key, value] of Object.entries(row)) {
          result[key] = result[key] || [];
          result[key].push(value);
        }
      }

      return result;
    }

    // Morris js data formulating.
    function morrisJs(wrapper, table, count) {
      // Chart settings.
      var options = {};
      // List of settings along with their type and default value.
      var optKeys = [
        ['colors', 'array', null],
        ['stacked', 'bool', false],
        ['resize', 'bool', false],
        ['type', 'str', 'line'],
        ['lineWidth', 'str', null],
        ['pointSize', 'str',null],
        ['pointFillColors', 'str', null],
        ['pointStrokeColors', 'str', null],
        ['ymax', 'str', null],
        ['ymin', 'str', null],
        ['smooth', 'bool', true],
        ['hideHover', 'bool', false],
        ['hoverCallback', 'str', null],
        ['parseTime', 'bool', true],
        ['postUnits', 'str', null],
        ['preUnits', 'str', null],
        ['dateFormat', 'str', null],
        ['xLabels', 'str', null],
        ['xLabelFormat', 'str', null],
        ['yLabelFormat', 'str', null],
        ['goals', 'array', null],
        ['goalStrokeWidth', 'str', null],
        ['goalLineColors', 'array', null],
        ['events', 'str', null],
        ['eventStrokeWidth', 'str', null],
        ['eventLineColors', 'array', null],
        ['continuousLine', 'str', null],
        ['axes', 'bool', true],
        ['grid', 'bool', true],
        ['gridTextColor', 'str', null],
        ['gridTextSize', 'int', null],
        ['gridTextFamily', 'str', null],
        ['gridTextWeight', 'str', null],
        ['fillOpacity', 'float', null],
        ['behaveLikeLine', 'bool', null],
        ['xkey', 'str', null],
        ['ykeys', 'array', []],
        ['labels', 'array', null]
      ];

      //alert('getting here now: ');
      options = generateOptionsList(optKeys, table, 'morris');
      options.element = 'morris-chart-' + count;
      // Data settings.

      // Ignoring the first column (i.e. 0) has some odd behavior. Zero (0)
      // being misinterpreted as the emtpy array value. So we treat this
      // case separately.
      if (table.data('tabletojson-ignoreColumns') === 0) {
        options.ignoreColumns = [0];
      }
      else {
        options.ignoreColumns = table.data('tabletojson-ignoreColumns') ? String(table.data('tabletojson-ignoreColumns')).split(',') : [];
      }
      // Same issue as above
      if (table.data('tabletojson-onlyColumns') === 0) {
        options.onlyColumns = [0];
      }
      else {
        options.onlyColumns = table.data('tabletojson-onlyColumns') ? table.data('tabletojson-onlyColumns').split(',') : null;
      }
      options.ignoreHiddenRows = table.data('tabletojson-ignoreHiddenRows') ? true === table.data('tabletojson-ignoreHiddenRows') : true;
      options.headings = table.data('tabletojson-headings') ? table.data('tabletojson-headings').split(',') : null;
      options.keys = [];
      // Data preperation

      // Ensure all the values are true integers and not string numbers
      options.ignoreColumns = options.ignoreColumns.map(function (x) {
        return parseInt(x, 10);
      });

      // Read the table data with the given configuration
      options.table_data = table.tableToJSON({
        ignoreColumns: options.ignoreColumns,
        onlyColumns: options.onlyColumns,
        ignoreHiddenRows: options.ignoreHiddenRows,
        headings: options.headings
      });

      // Get the list of keys
      for (var i in options.table_data) {
        for (var j in options.table_data[i]) {
          options.keys.push(j);
        }
        break;
      }

      // Prepare display
      table.addClass('hidden');
      wrapper.append('<div id="' + options.element + '" class="morris-chart" style="height: 250px;"></div> <a href="#" class="button toggle-table" title="'+ Drupal.t("Toggle Data Table") +'">' + Drupal.t("Show data") + '</a>');

      // Draw the chart with the given options
      drawChart(options);

      wrapper.find('.button.toggle-table').click(function(event){
        table.toggleClass('element-invisible');
        $(this).toggleClass('down');
        if (table.hasClass('element-invisible')) {
          $(this).text(Drupal.t('Show Data'));
        }
        else {
          $(this).text(Drupal.t('Hide Data'));
        }
        event.preventDefault();
      });
    }

    function generateOptionsList(optKeys, table, charting_type) {
      var options = [];
          // Extract settings from data attributes if provided.
      $.each(optKeys, function(idx, optData) {
        var key = optData[0],
        type = optData[1],
        defaultValue = optData[2];

        // Check for original camel case and lowercase data attribute names, as some browsers lowercase the attribute names.
        var attrKey = null;
        if (table.data(charting_type + '-' + key)) {
          attrKey = key;
        }
        else if (table.data(charting_type + '-' + key.toLowerCase())) {
          attrKey = key.toLowerCase();
        }

        if (attrKey !== null) {
          if (type == 'str') {
            options[key] = table.data(charting_type + '-' + attrKey);
          }
          else if (type == 'bool') {
            options[key] = !!table.data(charting_type + '-' + attrKey);
          }
          else if (type == 'int') {
            options[key] = parseInt(table.data(charting_type + '-' + attrKey), 10);
          }
          else if (type == 'float') {
            options[key] = parseFloat(table.data(charting_type + '-' + attrKey));
          }
          else if (type == 'array') {
            options[key] = table.data(charting_type + '-' + attrKey).split(',');
          }
        }
        else {
          options[key] = defaultValue;
        }
      });

      return options;
    }



    /**
     * Generate the chart with the given defaults
     */
    function drawChart(options) {
      // Set global settings
      var settings = {
        element: options.element,
        data: options.table_data,
        resize: options.resize
      };

      // Set per-type settings
      switch (options.type) {
        case 'bar':
          // Add optional settings
          if (null !== options.colors && options.colors != undefined) {
            settings.barColors = options.colors;
          }
          if (options.stacked) {
              settings.stacked = options.stacked;
          }
          else {
            settings.stacked = settings.stacked;
          }
          if (null !== options.hideHover) {
            settings.hideHover = options.hideHover;
          }
          if (null !== options.hoverCallback) {
            settings.hoverCallback = options.hoverCallback;
          }
          if (null !== options.axes) {
            settings.axes = options.axes;
          }
          if (null !== options.grid) {
            settings.grid = options.grid;
          }
          if (null !== options.gridTextColor) {
            settings.gridTextColor = options.gridTextColor;
          }
          if (null !== options.gridTextSize) {
            settings.gridTextSize = options.gridTextSize;
          }
          if (null !== options.gridTextFamily) {
            settings.gridTextFamily = options.gridTextFamily;
          }
          if (null !== options.gridTextWeight) {
            settings.gridTextWeight = options.gridTextWeight;
          }
          if (null !== options.hideHover) {
            settings.hideHover = options.hideHover;
          }

          // Add required settings
          if (null != options.xkey) {
            settings.xkey = options.xkey;
          }
          else {
            settings.xkey = options.keys.shift();
          }

          if (options.ykeys.length > 0) {
            settings.ykeys = options.ykeys;
          }
          else {
            settings.ykeys = options.keys;
          }

          if (null != options.labels) {
            settings.labels = options.labels;
          }
          else {
            settings.labels = settings.ykeys;
          }

          new Morris.Bar(settings);
          break;
        case 'donut':
          if (null !== options.colors && options.colors != undefined) {
            settings.colors = options.colors;
          }
          if (null !== options.formatter) {
            settings.formatter = options.formatter;
          }
          new Morris.Donut(settings);

          break;
        case 'area':
          // Add optional settings
          if (null !== options.behaveLikeLine) {
            settings.behaveLikeLine = options.behaveLikeLine;
          }

          // Area and Line share all the same configuration except the one option
          // So we fall into the default case and check at the end which type to
          // generate.
        case 'line':
        default:
          // Add optional settings

          if (null !== options.colors && options.colors != undefined) {
            settings.lineColors = options.colors;
          }

          // Add required settings
          if (null != options.xkey) {
            settings.xkey = options.xkey;
          }
          else {
            settings.xkey = options.keys.shift();
          }

          if (options.ykeys.length > 0) {
            settings.ykeys = options.ykeys;
          }
          else {
            settings.ykeys = options.keys;
          }

          if (null != options.labels) {
            settings.labels = options.labels;
          }
          else {
            settings.labels = settings.ykeys;
          }

          // @todo adjust line settings
          if (options.type == 'area') {
            new Morris.Area(settings);
          }
          else {
            new Morris.Line(settings);
          }
      }
    }
  }(jQuery));

